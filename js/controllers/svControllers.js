function onLoading() {
  document.getElementById("loading").style.display = "flex";
}
function offLoading() {
  document.getElementById("loading").style.display = "none";
}

function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value.trim();
  var tenSv = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matKhau = document.getElementById("txtPass").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value.trim();
  var diemLy = document.getElementById("txtDiemLy").value.trim();
  var diemHoa = document.getElementById("txtDiemHoa").value.trim();
  var maSv = document.getElementById("txtMaSV").value.trim();

  var newSv = new SinhVien(
    maSv,
    tenSv,
    email,
    matKhau,
    diemToan,
    diemLy,
    diemHoa
  );
  return newSv;
}

function renderDssv(studentList) {
  var contentHTML = "";
  for (var index = 0; index < studentList.length; index++) {
    var currentSV = studentList[index];
    var result = `
        <tr>
        <td>${currentSV.ma}</td>
        <td>${currentSV.ten}</td>
        <td>${currentSV.email}</td>
        <td>${currentSV.tinhDTB()}</td>
        <td>
        <button class="btn btn-danger" onclick="xoaSv('${
          currentSV.ma
        }')">Xoá</button>
        <button class="btn btn-success" onclick="suaSv('${
          currentSV.ma
        }')">Sửa</button>
        </td>
        </tr>
        `;
    contentHTML += result;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}
function disabledInput(idSv) {
  document.getElementById(idSv).disabled = true;
}
function enabledInput(idSv) {
  document.getElementById(idSv).disabled = false;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}
