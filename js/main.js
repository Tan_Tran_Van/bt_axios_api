// //pedding, resolve, reject
// //axios ~ bất đồng bộ
// const BASE_URL = "https://633ec04e83f50e9ba3b76077.mockapi.io";

// // lay danh sach sinh tu service
// var fetchDssvService = function () {
//   onLoading();
//   axios({
//     url: `${BASE_URL}/sv`,
//     method: "GET",
//   })
//     .then(function (response) {
//       console.log("response: ", response.data);
//       offLoading();
//       renderDSSV(response.data);
//     })
//     .catch(function (error) {
//       console.log("error: ", error);
//       offLoading();
//     });
// };
// //load data tu service
// fetchDssvService();

// //xoá sinh vien
// var xoaSv = function (idSv) {
//   onLoading();
//   axios({
//     url: `${BASE_URL}/sv/${idSv}`,
//     method: "DELETE",
//   })
//     .then(function (response) {
//       fetchDssvService();
//       offLoading();
//       Swal.fire("Xoá thành công");
//     })
//     .catch(function (error) {
//       offLoading();
//       Swal.fire("Xoá thất bại");
//     });
// };
// //thêm sinh viên
// var themSv = function () {
//   var newSv = layThongTinTuForm();
//   console.log("newSv: ", newSv);

//   axios({
//     url: `${BASE_URL}/sv`,
//     method: "POST",
//     data: newSv,
//   })
//     .then(function (res) {
//       fetchDssvService();
//       Swal.fire("Thêm thành công");
//     })
//     .catch(function (err) {
//       Swal.fire("Thêm thất bại");
//     });
// };

// //Sua Sinh vien

// var layThongTinSinhVien = function (idSv) {
//   axios({
//     url: `${BASE_URL}/sv/${idSv}`,
//     method: "GET",
//   })
//     .then(function (res) {
//       Swal.fire("Lấy thông tin thành công");
//     })
//     .catch(function (err) {
//       Swal.fire("Lấy thông tin thất bại");
//     });
// };

// var suaSv = function (idSv) {
//   layThongTinSinhVien(idSv);
// };

// var capNhatSv = function () {
//   var svEdit = layThongTinTuForm();
//   axios({
//     url: `${BASE_URL}/sv/${svEdit.ma}`,
//     method: "PUT",
//   })
//     .then(function (res) {
//       Swal.fire("Lấy thông tin thành công");
//     })
//     .catch(function (err) {
//       Swal.fire("Lấy thông tin thất bại");
//     });
// };

const BASE_URL = "https://633ec04e83f50e9ba3b76077.mockapi.io";

//fetch Student List from Service

function fetchStudentListFormService() {
  onLoading();
  axios({
    url: `${BASE_URL}/qlsv`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      // Swal.fire("Lấy dữ liệu thành công");
      var studentList = res.data.map(function (sv) {
        return new SinhVien(
          sv.ma,
          sv.ten,
          sv.email,
          sv.matKhau,
          sv.diemToan,
          sv.diemLy,
          sv.diemHoa
        );
      });
      renderDssv(studentList);
    })
    .catch(function (err) {
      offLoading();
      // Swal.fire("Lấy dữ liệu thất bại!");
    });
}
fetchStudentListFormService();

//delete student

function xoaSv(idSv) {
  onLoading();
  axios({
    url: `${BASE_URL}/qlsv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      offLoading();
      Swal.fire("Xoá Sinh viên thành công!");
      fetchStudentListFormService();
    })
    .catch(function (err) {
      offLoading();
      Swal.fire("Xoá Sinh viên thất bại!");
    });
}

// create SV
function themSv() {
  onLoading();
  var newSV = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/qlsv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      offLoading();
      Swal.fire("Thêm Sinh viên thành công!");
      fetchStudentListFormService();
    })
    .catch(function (err) {
      offLoading();
      Swal.fire("Thêm Sinh viên thất bại!");
    });
}

// replace and update SV

function suaSv(idSv) {
  onLoading();
  axios({
    url: `${BASE_URL}/qlsv/${idSv}`,
    method: "GET",
  })
    .then(function (res) {
      offLoading();
      disabledInput("txtMaSV");
      disabledInput("btnThemSv");
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      offLoading();
      Swal.fire("Chọn Sinh viên thất bại!");
    });
}
function capNhatSv() {
  onLoading();
  var svEdit = layThongTinTuForm();

  axios({
    url: `${BASE_URL}/qlsv/${svEdit.ma}`,
    method: "PUT",
    data: svEdit,
  })
    .then(function (res) {
      offLoading();
      Swal.fire("Cập nhật Sinh viên thành công!");
      enabledInput("txtMaSV");
      enabledInput("btnThemSv");
      resetForm();
      fetchStudentListFormService();
    })
    .catch(function (err) {
      offLoading();
      resetForm();
      enabledInput("txtMaSV");
      enabledInput("btnThemSv");
      Swal.fire("Cập nhật Sinh viên thất bại!");
    });
}
//find SV
document.getElementById("btnSearch").onclick = function () {
  var nameSv = document.getElementById("txtSearch").value;
  console.log("nameSv: ", nameSv);
  axios({
    url: `${BASE_URL}/qlsv`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res.data: ", res.data);
      var findSv = res.data.filter(function (sv) {
        return sv.ten.toLowerCase().indexOf(nameSv.toLowerCase()) > -1;
      });
      if (findSv.length == 0) return Swal.fire("Không tìm thấy Sinh viên!");
      console.log("findSv: ", findSv.length);
      var studentList = findSv.map(function (sv) {
        return new SinhVien(
          sv.ma,
          sv.ten,
          sv.email,
          sv.matKhau,
          sv.diemToan,
          sv.diemLy,
          sv.diemHoa
        );
      });
      renderDssv(studentList);
      console.log("studentList: ", studentList);
      offLoading();
    })
    .catch(function (err) {
      offLoading();
      Swal.fire("Lấy dữ liệu thất bại!");
    });
};

//reset form SV
function resetFormSv() {
  resetForm();
  enabledInput("txtMaSV");
  enabledInput("btnThemSv");
  document.getElementById("txtSearch").value = "";
  fetchStudentListFormService();
}
